'use strict';
module.exports = function(sequelize, DataTypes) {
    var Equipo = sequelize.define('Equipo', {
        idEquipo: {
            type: DataTypes.INTEGER,
            primaryKey: true,
            allowNull: false,
            autoIncrement: true
        },
        nombre: {
            type: DataTypes.STRING(200),
            allowNull: false,
        },
        pais: {
            type: DataTypes.STRING(200),
            allowNull: false,
        },
    }, {
        timestamps: false,
        underscored: true,
        freezeTableName: true,
        tableName: 'equipos',
        classMethods: {
            associate: function(models) {
                models.Equipo.hasMany(models.Ciclista, { 'foreignKey': 'idEquipo' });
            }
        }
    });
    return Equipo;
};