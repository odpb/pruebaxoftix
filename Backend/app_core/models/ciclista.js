'use strict';
module.exports = function(sequelize, DataTypes) {
    var Ciclista = sequelize.define('Ciclista', {
        idCiclista: {
            type: DataTypes.INTEGER,
            primaryKey: true,
            allowNull: false,
            autoIncrement: true
        },
        nombre: {
            type: DataTypes.STRING(200),
            allowNull: false,
        },
        equipo: {
            type: DataTypes.INTEGER,
            allowNull: false
        }
    }, {
        timestamps: false,
        underscored: true,
        freezeTableName: true,
        tableName: 'ciclistas',
        classMethods: {
            associate: function(models) {
                models.Ciclista.belongsTo(models.Equipo, { 'foreignKey': 'equipo' });
            }
        }
    });
    return Ciclista;
};