'use strict';
module.exports = function(sequelize, DataTypes) {
    var Competicion = sequelize.define('Competicion', {
        idCompeticion: {
            type: DataTypes.INTEGER,
            primaryKey: true,
            allowNull: false,
            autoIncrement: true
        },
        nombre: {
            type: DataTypes.STRING(200),
            allowNull: false
        },
        pais: {
            type: DataTypes.STRING(25),
            allowNull: false
        },
        duracion: {
            type: DataTypes.STRING(50),
            allowNull: false
        }
    }, {
        timestamps: false,
        underscored: true,
        freezeTableName: true,
        tableName: 'competiciones',
        classMethods: {
            associate: function(models) {
                models.Competicion.hasMany(models.Ciclista, { 'foreignKey': 'idClasificacion' });
            }
        }
    });
    return Competicion;
};