var Models = require("../models/index");
var sequelize = Models.sequelize;
var Q = require("q");

var create = function(equipo) {
    return Models.Equipo.create({
        nombre: equipo.nombre,
        pais: equipo.pais

    });
};

module.exports.create = create;