var Models = require("../models/index");
var sequelize = Models.sequelize;
var Q = require("q");

var create = function(ciclista) {
    return Models.Ciclista.create({
        nombre: ciclista.nombre,
        equipo: ciclista.equipo
    });
};


var findById = function(opcion) {
    return Models.Ciclista.find({
        where: {
            idCiclista: opcion
        }
    });
};

var deleteById = function(identificador) {
    return Models.Ciclista.destroy({
        where: {
            idCiclista: identificador
        }
    });
};

var findAll = function() {
    return Models.Ciclista.findAll({});
};

module.exports.create = create;
module.exports.deleteById = deleteById;
module.exports.findAll = findAll;
module.exports.findById = findById;