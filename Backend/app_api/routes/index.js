var Express = require('express');

var controllerCiclista = require('../controllers/ciclistaController');

var router = Express.Router();

//CICLISTA
router.get('/buscarCiclista/:idCiclista', controllerCiclista.findById);
router.get("/buscarCiclistas/", controllerCiclista.findAll);
router.post("/agregarCiclista/", controllerCiclista.addCiclista);
router.put("/borrarCiclista/:idCiclista", controllerCiclista.deleteCiclista);
//router.post("/actulizarCiclista/", controllerCiclista);

module.exports = router;