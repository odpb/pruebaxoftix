var CiclistaDao = require("../../app_core/dao/ciclistaDao");
var Respuesta = require("../../app_core/helpers/respuesta");


var findAll = function(req, res) {
    CiclistaDao.findAll().then(function(ciclistas) {
        Respuesta.sendJsonResponse(res, 200, ciclistas);
    }).catch(function(error) {
        Respuesta.sendJsonResponse(res, 500, { "error": 'se ha producido un error en la consulta' })
    });
};

var findById = function(req, res) {
    CiclistaDao.findById(req.params.id).then(function(ciclista) {
        Respuesta.sendJsonResponse(res, 200, ciclista);
    }).catch(function(error) {
        Respuesta.sendJsonResponse(res, 200, { "error": 'se ha producido un error en la consulta' });
    });
};

var addCiclista = function(req, res) {
    var ciclista = {
        "nombre": req.body.nombre,
        "equipo": req.body.equipo
    };
    CiclistaDao.create(ciclista).then(function(ciclista) {
        Respuesta.sendJsonResponse(res, 200, ciclista)
    }).catch(function(error) {
        Respuesta.sendJsonResponse(res, 500, error);
    });
};

var deleteCiclista = function(req, res) {
    CiclistaDao.deleteById(req.params.id).then(function(ciclista) {
        if (ciclista == 1) {
            Respuesta.sendJsonResponse(res, 200, { "mensaje": "registro eliminado" });
        } else {
            Respuesta.sendJsonResponse(res, 404, { "mensaje": "registro no encontrado" });
        }
    }).catch(function(error) {
        Respuesta.sendJsonResponse(res, 500, error);
    });
};

module.exports.addCiclista = addCiclista;
module.exports.deleteCiclista = deleteCiclista;
module.exports.findAll = findAll;
module.exports.findById = findById;