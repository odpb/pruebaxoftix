CREATE DATABASE "PruebaExamen";

CREATE TABLE Equipos(
    idEquipo SERIAL PRIMARY KEY,
    nombre VARCHAR(50) NOT NULL,
    pais VARCHAR(50) NOT NULL

);

CREATE TABLE Ciclistas(
    idCiclista SERIAL PRIMARY KEY,
    nombre VARCHAR(50) NOT NULL,
    equipo SMALLINT FOREIGN KEY REFERENCES Equipos.idEquipo
);

CREATE TABLE Clasificacion(
    idCiclistas SMALLINT FOREIGN KEY REFERENCES Ciclistas.idCiclista,
    idCompeticion SMALLINT FOREIGN KEY REFERENCES Competiciones.idCompeticion,
    puesto SMALLINT NOT NULL UNIQUE
);

CREATE TABLE Competiciones(
    idCompeticion SERIAL PRIMARY KEY,
    nombre VARCHAR(50) NOT NULL,
	duracion INT NOT NULL
);

INSERT INTO CICLISTAS (nombre, equipo) VALUES ('Nairo Quintana','1');
INSERT INTO CICLISTAS (nombre, equipo) VALUES ('Mikel LANDA','1');
INSERT INTO CICLISTAS (nombre, equipo) VALUES ('Imanol ERVITTI','1');

INSERT INTO CICLISTAS (nombre, equipo) VALUES ('Rigoberto Urán','5');
INSERT INTO CICLISTAS (nombre, equipo) VALUES ('Tom Scully','5');
INSERT INTO CICLISTAS (nombre, equipo) VALUES ('Sebastian Langeveld','5');

INSERT INTO CICLISTAS (nombre, equipo) VALUES ('Sergio Luis Henao','6');
INSERT INTO CICLISTAS (nombre, equipo) VALUES ('Dan Martin','6');
INSERT INTO CICLISTAS (nombre, equipo) VALUES ('Rui Costa','6');
commit;